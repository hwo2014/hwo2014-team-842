var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
//var botName = process.argv[4] + '_' + process.argv[6];
var botName = "enpatech_" + (Math.random() * 100000).toFixed(0);
var botKey = process.argv[5];
var cmd = process.argv[6];
var baseThrottle = parseFloat(process.argv[7]) || 0;
var currentlyCrashed = false;
//console.log(botThrottle);

var botThrottle = 0.0;
var lastAngle = 0.0;
var speed = 0;
var lastPosition = 0;
var gameData = {};
var numberOfPieces = 0;

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
	return send({
	msgType: "join",
	data: {
	  name: botName,
	  key: botKey
	}
  });
	/*return send({
		"msgType": cmd,
		"data": {
			"botId": {
				"name": botName,
				"key": botKey
			},
			"trackName": "keimola",
			"password": "testblabla5464654",
			"carCount": 1
		}
	});*/
});

function send(json) {
	client.write(JSON.stringify(json));
	return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
	//console.log(data);
	if (data.msgType === 'carPositions') {
		for (var c = 0, l = data.data.length; c < l; ++c) {
			var car = data.data[c];
			if (car.id.name == botName) {
				var angle = Math.abs(car.angle);
				var angleDelta = angle - lastAngle
				//botThrottle = 0.8 - (angle / 100) + baseThrottle;
				
				var position = getPositionOnTrack(car);
				var cSpeed = position - lastPosition;
				var speedDelta = cSpeed - speed;
				speed = cSpeed;
				lastPosition = position;
				lastAngle = angle;

				var rad = radiusOfPieceAndLane(car.piecePosition.pieceIndex, car.piecePosition.lane.endLaneIndex);
				var radOfNextPiece = radiusOfPieceAndLane(car.piecePosition.pieceIndex + 1, car.piecePosition.lane.endLaneIndex);
				var radOfNextNextPiece = radiusOfPieceAndLane(car.piecePosition.pieceIndex + 2, car.piecePosition.lane.endLaneIndex);

				//var nRad = radiusOfPieceAndLane((car.piecePosition.pieceIndex+1)%40, car.piecePosition.lane.endLaneIndex);

				!currentlyCrashed && console.log(position.toFixed(2), speed.toFixed(2), rad.toFixed(2), angle.toFixed(2), angleDelta.toFixed(2));
				botThrottle = 0.95;
				console.log("standard");

				if (radOfNextPiece == 9999999 && radOfNextNextPiece == 9999999) {
					botThrottle = 1.0;
					console.log("-->Full Accelleration");
				} else if (radOfNextPiece == 9999999 && radOfNextNextPiece != 9999999 && speed >= 8.0) {
					botThrottle = 0.7;
					console.log("-->lower full spead");
				} 

				if (radOfNextPiece <= 110 && speed > 7.5) {
					botThrottle = 0.45;
					console.log("-->harder break")
				} else if (radOfNextPiece != 9999999 && speed >= 8.0) {
					botThrottle = 0.75;
					console.log("-->soft break<--");
				} else if ((rad <= 110 && rad >= 100 && speed > 7.0 ) || (rad < 100 && speed > 6.5 ) || 
					(rad <= 150 && rad > 110 && speed > 7.7 ) || (rad <= 200 && rad > 150 && speed > 8.0)) {
					botThrottle = 0.0;
					console.log("-->BREAK!!<--");
				} 

				
			}
		}
		send({
			msgType: "throttle",
			data: botThrottle
		});
	} else {
		if (data.msgType === 'join') {
			console.log('Joined')
		} else if (data.msgType === 'gameStart') {
			console.log('Race started');
		} else if (data.msgType === 'gameEnd') {
			console.log('Race ended');
		} else if (data.msgType == 'lapFinished') {
			console.log('Lap finished');
		} else if (data.msgType == 'crash') {
			console.log('Crashed!');
			currentlyCrashed = true;
		} else if (data.msgType == 'spawn') {
			console.log('Spawned!');
			currentlyCrashed = false;
		} else if (data.msgType == 'gameInit') {
			gameData = data.data;
			numberOfPieces = gameData.race.track.pieces.length;
			console.log("number of pieces: " + gameData.race.track.pieces.length);
			console.log(gameData.race.track.pieces);
			console.log(gameData.race.track.lanes);
		}

		send({
			msgType: "ping",
			data: {}
		});
	}
});

jsonStream.on('error', function() {
	return console.log("disconnected");
});

function getPositionOnTrack(car) {
	var pi = car.piecePosition.pieceIndex,
		inPiDi = car.piecePosition.inPieceDistance,
		laneIndex = car.piecePosition.lane.endLaneIndex;

	return laneLengthToPiece(pi,laneIndex) + inPiDi;
}

function laneLengthToPiece(i, laneId) {
	var length = 0;
	for ( var c = 0; c < i; ++c ) {
		length += lengthOfPiece(c,laneId);
	}
	return length;
}

function lengthOfPiece(i, laneId) {
	var piece = gameData.race.track.pieces[i],
		length = piece.length;
	if ( !length )  {
		var radius = radiusOfPieceAndLane(piece, laneId),
			angle = Math.abs(piece.angle),
			length = 2 * Math.PI * radius * (angle / 360);
	}
	return length;
}

function radiusOfPieceAndLane(pieceOrI, laneId) {
	if (pieceOrI >= numberOfPieces) {
		return 9999999;
	}
	if ( typeof pieceOrI == 'number' ) {
		pieceOrI = gameData.race.track.pieces[pieceOrI];
	}
	var piece = pieceOrI;
	if (!piece.angle) {
		return 9999999;
	}

	var centerDistance = piece.angle < 0 ? gameData.race.track.lanes[laneId].distanceFromCenter : -gameData.race.track.lanes[laneId].distanceFromCenter,
		radius = piece.radius + centerDistance;
	
	return radius;
}